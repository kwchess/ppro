FROM node:10.8-slim
EXPOSE 7777
COPY package*.json ./
RUN npm install
COPY index.js .
COPY src src
CMD node index.js
