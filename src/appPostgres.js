const express = require('express');
const app = express();

module.exports = db => {
  app.get('/', async (req, res) => {
    console.log ( "Recd Request ////") ;
    db.connect((err, client, release) => {
      if (err) {
        return console.error('Error acquiring client', err.stack)
      }
      client.query('SELECT * FROM HelloWorld LIMIT 1', (err, result) => {
        release()
        if (err) {
          return console.error('Error executing query', err.stack)
        }
        console.log(result.rows)
        res.status(200).send(result.rows[0]) ;
      })
    })
  });
  return app;
};
