#### Objective
1. Develop an Application
2. Host the code on GitLab
3. Use GitLab Pipelines to automate deployments

#### Setup Overview
Currently the application is hosted on Digital Ocean Kubernetes Environment
Both dev & prod currently runs on same kubernetes environment and is isolated using namespace.
There are 2 branches in the gitlab source control
  develop branch for development
  master branch for production 
  
All commands provided were tested on Mac ( may work on Linux )

<<<<<<< HEAD
=======
##### Directory Structure
  helloworld => Helm Chart for this application
  setup      => contains custom values for gitlab-runner and RBAC yaml file
          

>>>>>>> develop
##### Prerequisites
1. A kubernetes environment
2. An ingress controller running on kubernetes 
    Pls follow the installation guide for the relevant cloud environment
    https://kubernetes.github.io/ingress-nginx/deploy/

##### Setup on Kubernetes
###### Create 2 namespaces ( dev & prod )
```
     kubectl create ns dev
     kubectl create ns prod
```

##### Create Docker Login ConfigMap
###### For each environment ( namespace )
```
kubectl create secret generic docker-registry gitlabsecrets --docker-server=registry.gitlab.com --docker-username=<username> --docker-password=<password> --docker-email=<email> -n <namespace>
```

##### GitLab Configuration

###### Add Environments

Click on Environment from Operations on the Left Plane.
Create 2 environments ( deveand prod )

###### Add Variables

Variables can be setup by navigating to Settings ( on the left panel ) => CI/CD

1. kube_config
create a variable "kube_config" with the output of the below command
Go to terminal and issue the following command
```
echo $(cat ~/.kube/config | base64) | tr -d " "
```

2. CI_BUILD_TOKEN
Click on Profile ( right top corner ) => Settings 
On the Left Panel click on "Access Token"
Provide any name eg. "ppro"
Choose Scopes ( read_repository, write_repository, read_registry, write_registry )
Click on "Create Personal Token" button
Copy the generated token in "Your new personal access token" 
Create a variable "CI_BUILD_TOKEN" and set the value to the copied personal access token.

##### Install GitLab Runner on Kubernetes

Go to Settings ( on the left panel ) => CI/CD
Click on Expand button of "Runners" note down the Registration Token.

Install GitLab Runner using the Registration Token

```
cd setup
helm repo add gitlab https://charts.gitlab.io
helm install gr gitlab/gitlab-runner -f gitlab-runner.values.yaml --set runnerRegistrationToken=<RegistrationToken>
```

#### Access dev environment
Enable Port Forwarding ( in the terminal )
kubectl port-forward svc/hw-helloworld 7777 -n prod &
sleep 5
http://localhost:7777
{"error_code":"NOT_FOUND_ERROR","message":"No Records"}
http://localhost:7777/test
{"error_code":"OK","message":"Created Test Data"}
http://localhost:7777
[{"Message":"Hello World from DB"}]

#### Access prod environment ( using ingress IP )
Main Site       => http://<ip>
Load Test Data  => http://<ip>/test

#### Deploy PostgreSQL on Kubernetes

https://severalnines.com/database-blog/using-kubernetes-deploy-postgresql

#### Install PostgreSQL Client
Installation Document of psql 
https://blog.timescale.com/tutorials/how-to-install-psql-on-mac-ubuntu-debian-windows/

```
cd setup
./setupDB4Environment.sh <namespace>
```
You will be get postgres prompt "postgres=#" 
past the below to create the initial table and insert a sample row
```
CREATE TABLE HelloWorld ( Message TEXT ) ;
insert into HelloWorld ( message ) values ( 'Hello WORLD !!!' ) ;
exit ;
```
