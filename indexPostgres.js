const PORT = 7777;
const Pool = require('pg').Pool

const main = async () => {
  try {
    const db = new Pool ({
      host: process.env.PGHOST,
      port: parseInt(process.env.PGPORT),
      user: process.env.PGUSER,
      password: process.env.PGPASSWORD,
      database: process.env.PGDATABASE,
      max: 10
    });    
    const app = require('./src/app')(db);

    app.listen(port, () => console.log(`App started and listening on port ${PORT}`));
  } catch (error) {
    throw Error('can not access database');
  }
};

main();
