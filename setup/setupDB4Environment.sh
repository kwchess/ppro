if [ $# -eq 0 ]; then
    echo "<namespace> is missing"
    exit 1
fi
ENVIRONMENT=$1

helm repo add bitnami https://charts.bitnami.com/bitnami

helm install db bitnami/postgresql -n ${ENVIRONMENT} --wait 
export PGPASSWORD=$(kubectl get secret -n ${ENVIRONMENT} db-postgresql -o jsonpath="{.data.postgresql-password}" | base64 --decode)
export PGHOST=db-postgresql
export PGUSER=postgres
export PGDATABASE=postgres

cat <<EOF >/tmp/db-config-${ENVIRONMENT}.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  labels:
    app: db-config
  name: db-config
data:
  PGDATABASE: ${PGDATABASE}
  PGHOST: ${PGHOST}
  PGPASSWORD: ${PGPASSWORD}
  PGPORT: "5432"
  PGTZ: "Asia/Singapore"
  PGUSER: ${PGUSER}
  DB_POOL_SIZE: "2"
EOF

kubectl delete cm db-config -n ${ENVIRONMENT}
kubectl apply -f /tmp/db-config-${ENVIRONMENT}.yaml -n ${ENVIRONMENT}

