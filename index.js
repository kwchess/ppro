const port = 7777;
const sqlite3 = require('sqlite-async');

const buildSchemas = require('./src/schemas');

const main = async () => {
  try {
    const db = await sqlite3.open(':memory:');
    buildSchemas(db);
    const app = require('./src/app')(db);

    app.listen(port, () => console.log(`App started and listening on port ${port}`));
  } catch (error) {
    throw Error('can not access sqlite database');
  }
};

main();
